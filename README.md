# AJAX y JQuery

En este repositorio tendremos los basico de jquery para manejar algunos aspectos del DOM y como realizar peticiones asyncronas mediante ajax, para poder actulizar nuestra página web sin la necesidad de recargarla cuando sea necesario. El repositorio ya incluye la libreria JQuery.

Esta libreria debe ser incluida entre las etiquetas "head"
```
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="./jquery.min.js"></script>
	<title>AJAX y JQuery</title>
</head>
```

Prof. Andres D. Romano